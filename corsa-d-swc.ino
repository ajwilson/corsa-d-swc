#include <SPI.h>
#include "mcp2515_can.h"

#define CAN_ID_SWC    (0x206)
#define CAN_BAUD_RATE (CAN_95KBPS)
#define SWC_PRESS     (1)
#define SWC_RELEASE   (0)

// Set SPI CS Pin according to your hardware
const int SPI_CS_PIN = 10;
const int CAN_INT_PIN = 2;

mcp2515_can CAN(SPI_CS_PIN); // Set CS pin                          

void setup() {
    SERIAL_PORT_MONITOR.begin(115200);

    while (CAN_OK != CAN.begin(CAN_BAUD_RATE)) {             // init can bus : baudrate = 500k
        SERIAL_PORT_MONITOR.println("CAN init fail, retry...");
        delay(100);
    }
    SERIAL_PORT_MONITOR.println("CAN init ok!");

    /* Filter on useful messages */
    CAN.init_Filt(0,0,CAN_ID_SWC);
}


void loop() {
    unsigned char len = 0;
    unsigned char buf[8];

    if (CAN_MSGAVAIL == CAN.checkReceive()) {
        CAN.readMsgBuf(&len, buf);

        unsigned long canId = CAN.getCanId();

        if( canId == CAN_ID_SWC )
        {
          // buf[0] = pressed or not
          // buf[1] = button
          // buf[2] = incremented when holding
          
          if( buf[0] == SWC_PRESS )
          {
            Serial.print( "Pressed " );
          }
          else
          {
            Serial.print("Released ");
          }

          switch(buf[1])
          {  
            case 0x81:
              Serial.println("Mode, Phone Up");
              break;
            case 0x82:
              Serial.println("Aps, Phone Down");
              break;
            case 0x83:
              Serial.println("Seek Change");
              break;
            case 0x84:
              Serial.println("Seek Press");
              break;
            case 0x91:
              Serial.println("Button Up");
              break;
            case 0x92:
              Serial.println("Button Down");
              break;
            case 0x93:
              Serial.println("Volume Change");
              break;
            default:
              Serial.println(buf[1]);
              break; 
          }

          // for vol/seek 1 is up
          // 255 is down

          if( buf[2] > 0 )
          {
            Serial.println((String)buf[2] + " times");
          }
        }
    }
}

/*********************************************************************************************************
    END FILE
*********************************************************************************************************/

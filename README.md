# Corsa D SWC via CAN Bus

This Arduino sketch is used to log SWC information from the Medium Speed CAN Bus on Vauxhall/Opel Corsa D models.

The MCP2515 module was used for this, specifically one with a 16mhz crystal as most of the libraries are setup with the timings for 16mhz.

Medium Speed CAN on the vehicle uses 95kbps and can be access via the OBD II port or from the CAN H/CAN L behind the CD30 radio which can easily be spliced.

The CAN ID 0x206 contains the data for which steering wheel button has been pressed as well as for how long and if the button was pressed/released.

| Data Byte | Purpose                                                      |
| --------- | ------------------------------------------------------------ |
| 0         | 0 = Released<br />1 = Pressed                                |
| 1         | Button ID                                                    |
| 2         | Incremented when button is held<br />For the seek/volume this will be 1 when pushed up and 255 when pushed down |

Here's a list of button IDs that exist on my vehicle, it's possible on other models there's more.

| Button ID | Description    |
| --------- | -------------- |
| 0x81      | Phone Pick Up  |
| 0x82      | Phone Hang Up  |
| 0x83      | Seek Direction |
| 0x84      | Seek Pressed   |
| 0x91      | Button Up      |
| 0x92      | Button Down    |
| 0x93      | Volume Change  |

